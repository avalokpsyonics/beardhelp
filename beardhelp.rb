system("ifconfig")
puts "Enter the target interface name."
uin = gets

puts "Detecting internal IP..."
iip = `ifconfig enp11s0 | grep 'inet ' | cut -d: -f3 | awk '{ print $2}'`.chomp

puts "Detecting external IP..."
eip = `curl ipinfo.io/ip`.chomp

puts "Your external IP is #{eip}."

puts "Installing ssh..."
system("sudo apt-get install openssh-server")
puts "openssh-server installed."

puts "Forward port 2222 on #{eip} at 192.168.0.1 to port 22 on #{iip}"

